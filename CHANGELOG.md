# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.7

- patch: jre 8

## 0.1.6

- patch: java 10

## 0.1.5

- patch: back to java 8

## 0.1.4

- patch: cleanup

## 0.1.3

- patch: Added mercurial and improved some things.

## 0.1.2

- patch: Upgraded to JDK 11

## 0.1.1

- patch: added nodejs and git

## 0.1.0

- minor: Initial commit

